###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01d - crazy cat guy - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
###
###
### @author  Arthur Lee <leea3@hawaii.edu>
### @date    12 Jan 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = crazyCatGuy

all: $(TARGET)

crazyCatGuy: crazyCatGuy.c
	$(CC) $(CFLAGS) -o $(TARGET) crazyCatGuy.c

clean:
	rm -f $(TARGET) *.o

